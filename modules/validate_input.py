from datetime import datetime

def actual_date():
    now_date = datetime.now()
    now_date_str = now_date.strftime('%Y-%m-%d')
    return now_date_str

def validate_date(items):
    try:
        datetime.strptime(items, '%Y-%m-%d')

    except ValueError:
        raise ValueError("Niepoprawny format daty, proszę podać w formacie: YYYY-MM-DD")

def date_is_future(items):
    if items > actual_date():
        #print('poprawa data')
        return items           
    else:
        print('Podana data jest przeszła, proszę spróbować ponownie.')
        exit()

def step_by_step(items):
    actual_date()
    validate_date(items)
    date_is_future(items)
