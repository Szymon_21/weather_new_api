import requests
import json
from modules.validate_input import step_by_step
import pickle
import pprint

dict_15_days = {}

class Weather_forecast:

    def __init__(self, api_key):
        self.api_key = api_key

    def get_data(self):
        api = f'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/Warsaw/?key={self.api_key}'
        self.json_data = requests.get(api).json()

    def open_file(self):
        with open('weather_forecast', 'r') as f:
            data = json.load(f)
            return data

    def save_data(self):
        with open('weather_forecast', 'w') as f:
            json.dump(self.json_data, f)

    def dict_save(self):
        dict_15_days_save = open('15_days_temp.pkl','wb')
        pickle.dump(dict_15_days, dict_15_days_save)
        dict_15_days_save.close()

    def __iter__(self):
        try:       
            index = 0    
            for index in range(15):
                self.date = self.open_file()['days'][index]['datetime']
                self.temp_c = round((self.open_file()['days'][index]['temp'] - 32 )/ 1.8)
                
                dict_15_days[self.date] = self.temp_c
                index += 1

            self.dict_save()

        except:
            index = 0 
            for index in range(15):
                self.get_data()

                self.date = self.json_data['days'][index]['datetime']
                self.temp_c = round((self.json_data['days'][index]['temp'] - 32 )/ 1.8)

                dict_15_days[self.date] = self.temp_c
                self.save_data()
                index += 1

            self.dict_save()

    def print_item(self):
        dict_15_days_read = open('15_days_temp.pkl', "rb")
        dict_15_days = pickle.load(dict_15_days_read)

        for date, temp in dict_15_days.items():
            print(f'{date} - {temp}℃')

    def __getitem__(self, items):
        if items == '':
            self.__iter__()
            
            print(10 * '#')
            print('Temperatura na 15 dni: ')
            self.print_item()
            # dict_15_days_read = open('15_days_temp.pkl', "rb")
            # dict_15_days = pickle.load(dict_15_days_read)
            # pprint.pprint(dict_15_days)
            # self.print_item()
            # print(10 * '#')

        else:
            self.__iter__()
            step_by_step(items)

            dict_15_days_read = open('15_days_temp.pkl', "rb")
            dict_15_days = pickle.load(dict_15_days_read)

            if items in dict_15_days:
                pprint.pprint(f'Dnia {items}, będzie: {dict_15_days[items]}℃')
                self.print_item()
            else:
                print('Nie ma takiej daty.')